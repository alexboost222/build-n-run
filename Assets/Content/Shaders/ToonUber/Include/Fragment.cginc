#ifndef TOON_UBER_FRAGMENT
#define TOON_UBER_FRAGMENT

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"

#include "Properties.cginc"
#include "Structures.cginc"

fixed4 frag (const v2f i) : SV_Target
{
    const fixed4 tex = tex2D(_MainTex, i.uv);
    const fixed4 main_col = _MainColor * tex;
    const float3 view_dir = normalize(i.view_dir);
    const float3 half_vector = normalize(_MainLightPosition + view_dir);
    const float3 normal = normalize(i.world_normal);

    const float n_dot_l = dot(_MainLightPosition, normal);

    VertexPositionInputs vertex_input = (VertexPositionInputs)0;
    vertex_input.positionWS = i.world_position;
    
    const float4 shadow_coord = GetShadowCoord(vertex_input);
    const float shadow = MainLightRealtimeShadow(shadow_coord);

    const float light_intensity = smoothstep(0, _ShadowSmoothstep, n_dot_l * shadow);
    const float4 light = light_intensity * _MainLightColor;

    
    const float specular_intensity = pow(dot(normal, half_vector) * light_intensity, _Glossiness * _Glossiness);
    const float4 specular = smoothstep(0.005, 0.01, specular_intensity) * _SpecularColor;
    
    return main_col * (_AmbientColor + light + specular);
}

#endif 