#ifndef TOON_UBER_PROPERTIES
#define TOON_UBER_PROPERTIES

#include <HLSLSupport.cginc>

fixed4 _MainColor;
sampler2D _MainTex;
float4 _MainTex_ST;

fixed4 _AmbientColor;
float _ShadowSmoothstep;

float _Glossiness;
float4 _SpecularColor;

#endif