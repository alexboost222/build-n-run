#ifndef TOON_UBER_STRUCTURES
#define TOON_UBER_STRUCTURES

struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float2 uv : TEXCOORD0;
};

struct v2f
{
    float4 vertex : SV_POSITION;
    float2 uv : TEXCOORD0;
    
    float3 world_position : TEXCOORD1;
    float3 world_normal : TEXCOORD2;
    float3 view_dir : TEXCOORD3;
};

#endif