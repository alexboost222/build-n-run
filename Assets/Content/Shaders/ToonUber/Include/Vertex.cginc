#ifndef TOON_UBER_VERTEX
#define TOON_UBER_VERTEX

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
#include "Properties.cginc"
#include "Structures.cginc"

v2f vert (appdata v)
{
    v2f o;
    o.vertex = TransformObjectToHClip(v.vertex.xyz);
    o.world_position = TransformObjectToWorld(v.vertex.xyz);
    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
    o.world_normal = TransformObjectToWorldNormal(v.normal);
    o.view_dir = GetWorldSpaceViewDir(v.vertex);
    return o;
}


#endif