Shader "Unlit/ToonUberShader"
{
	Properties
	{
		[MainColor]
		_MainColor ("Main Color", Color) = (1, 1, 1, 1)
		
		[MainTexture]
		_MainTex ("Texture", 2D) = "white" {}
		
		[HDR]
		_AmbientColor("Ambient Color", Color) = (0.4,0.4,0.4,1)
		_ShadowSmoothstep("Shadow smoothness", Float) = 0.01
		
		[HDR]
		_SpecularColor("Specular Color", Color) = (0.9,0.9,0.9,1)
		_Glossiness("Glossiness", Float) = 32
	}
	SubShader
	{
		Tags 
		{ 
			"RenderType"="Opaque" 
			"LightMode" = "UniversalForward"	
			"PassFlags" = "OnlyDirectional"
		}
		LOD 100

		Pass
		{
			HLSLPROGRAM

			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS _MAIN_LIGHT_SHADOWS_CASCADE _MAIN_LIGHT_SHADOWS_SCREEN
            #pragma multi_compile_fragment _ _SHADOWS_SOFT
			
			#pragma vertex vert
			#pragma fragment frag

			#include "Include/Vertex.cginc"
			#include "Include/Fragment.cginc"
			
			ENDHLSL
		}
		
		UsePass "Universal Render Pipeline/Simple Lit/ShadowCaster"
	}
}