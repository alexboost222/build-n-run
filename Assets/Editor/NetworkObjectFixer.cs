using System.Linq;
using Unity.Netcode;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public static class NetworkObjectFixer
    {
        [MenuItem("Tools/Fix NetworkObjects")]
        public static void FixNetworkObjectsInScene()
        {
            var networkObjects = AssetDatabase.FindAssets("t:GameObject")
                .Select(guid => AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(guid)))
                .Where(obj => obj.TryGetComponent<NetworkObject>(out _))
                .OrderBy(o => o.name)
                .Select(go => go.GetComponent<NetworkObject>())
                .ToList();
                
            foreach (var networkObject in networkObjects)
            {
                var serializedObject = new SerializedObject(networkObject);
                var hashField = serializedObject.FindProperty("GlobalObjectIdHash");
         
                // Ugly hack. Reset the hash and apply it.
                // This implicitly marks the field as dirty, allowing it to be saved as an override.
                hashField.uintValue = 0;
                serializedObject.ApplyModifiedProperties();
                // Afterwards, OnValidate will kick in and return the hash to it's real value, which will be saved now.
            }
        }
    }
}