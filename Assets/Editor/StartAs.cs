using UnityEditor;
using UnityEngine;
using UnityToolbarExtender;

namespace Editor
{
    [InitializeOnLoad]
    public class StartAs
    {
        static StartAs()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
        }

        private static void OnToolbarGUI()
        {
            if (GUILayout.Button(EditorGUIUtility.IconContent("d_Linked", "Start as client")))
            {
                EditorPrefs.SetBool("IsServer", false);
                EditorApplication.EnterPlaymode();
            }
            
            GUILayout.FlexibleSpace();
        }
    
    }
}