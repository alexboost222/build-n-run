using System;
using UnityEngine;

namespace LowPolyVFX.Content.Scripts
{
    public class Grenade : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody rb;

        [SerializeField]
        private float force;

        [SerializeField]
        private GameObject spawnOnDeath;

        private void Start()
        {
            rb.AddForce(transform.forward * force, ForceMode.Impulse);
        }

        private void OnCollisionEnter(Collision other)
        {
            var contact = other.GetContact(0);
            Instantiate(spawnOnDeath, contact.point, Quaternion.identity).transform.forward = contact.normal;
            Destroy(gameObject);
        }
    }
}