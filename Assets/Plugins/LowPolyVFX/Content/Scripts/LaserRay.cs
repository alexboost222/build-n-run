using System;
using UnityEngine;

namespace LowPolyVFX.Content.Scripts
{
    public class LaserRay : MonoBehaviour
    {
        [SerializeField]
        private Transform rayImpact;
        
        [SerializeField]
        private LineRenderer lineRenderer;
        
        [SerializeField]
        private TrailRenderer trailRenderer;

        [SerializeField]
        private float trailDistance = .01f;

        private Vector3[] _positionsBuffer;
        private TrailRenderer _currentTrail;

        private void Awake()
        {
            _positionsBuffer = new Vector3[2];
        }

        private void Update()
        {
            if (Physics.Raycast(transform.position, transform.forward, out var hit, 100))
            {
                if (_currentTrail == null)
                    _currentTrail = Instantiate(trailRenderer);
                
                _currentTrail.transform.position = hit.point + hit.normal * trailDistance;

                _positionsBuffer[1] = Vector3.forward * hit.distance;

                rayImpact.transform.position = hit.point;
                rayImpact.transform.forward = hit.normal;
            }
            else
            {
                _currentTrail = null;
                
                _positionsBuffer[1] = Vector3.forward * 100;

                rayImpact.transform.position = lineRenderer.transform.position + Vector3.forward * 1000;
                rayImpact.transform.forward = Vector3.up;
            }

            // lineRenderer.SetPositions(_positionsBuffer);
        }
    }
}