using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LowPolyVFX.Content.Scripts
{
    [RequireComponent(typeof(Light))]
    public class LightFlickering : MonoBehaviour
    {
        public float flickeringRange;

        [Range(0, 1)]
        public float smoothness;
        
        private Light _light;
        private float _startIntensity;
        private void Start()
        {
            _light = GetComponent<Light>();
            _startIntensity = _light.intensity;
        }

        private void Update()
        {
            var target = Random.Range(_startIntensity - flickeringRange, _startIntensity + flickeringRange);

            _light.intensity = Mathf.Lerp(_light.intensity, target, smoothness);
        }
    }
}