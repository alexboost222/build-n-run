using System;
using UnityEngine;

namespace LowPolyVFX.Content.Scripts
{
    public class Projectile : MonoBehaviour
    {
        public float speed;

        [SerializeField] 
        private GameObject spawnOnDeath;

        private ParticleSystem _ps;
        private Rigidbody _rb;

        private void Start()
        {
            _ps = GetComponentInChildren<ParticleSystem>();
            _rb = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            _rb.position += transform.forward * (speed * Time.fixedDeltaTime);
        }

        private void OnCollisionEnter(Collision other)
        {
            _ps.Stop(true, ParticleSystemStopBehavior.StopEmitting);
            //_ps.transform.SetParent(null);

            if (spawnOnDeath != null)
            {
                var deathGo = Instantiate(spawnOnDeath, transform.position, Quaternion.identity);

                deathGo.transform.forward = other.contactCount > 0 ? other.contacts[0].normal : -transform.forward;
            }
            
            Destroy(gameObject);
        }
    }
}
