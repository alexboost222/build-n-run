using UnityEngine;

namespace LowPolyVFX.Content.Scripts
{
    public class ProjectileSpawner : MonoBehaviour
    {
        public Transform spawnPoint;
        public float maxLifetime;


        public void SpawnObject(Object obj)
        {
            Destroy(Instantiate(obj as GameObject, spawnPoint.position, spawnPoint.rotation), maxLifetime);
        }
    }
}
