﻿using Networking.Teams;
using UnityEngine;
using VContainer;

public class Altar : MonoBehaviour
{
    [SerializeField] private ETeam _team;

    public ETeam Team => _team;

    [Inject]
    public void Construct(TeamColorsService colorsService)
    {
        var col = colorsService.data[_team].mainTeamColor;
        col.a = 0.5f;

        GetComponent<Renderer>().material.color = col;
    }
}