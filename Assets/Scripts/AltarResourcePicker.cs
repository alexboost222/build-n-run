﻿using Networking;
using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using VContainer;
using Views;

namespace DefaultNamespace
{
    public class AltarResourcePicker : NetworkBehaviour
    {
        [SerializeField] private Collider _collider;
        [SerializeField] private CoinView _coinView;

        public readonly NetworkVariable<bool> HasCoin = new();

        private IPlayerDataService _playerDataService;
        private CoinsSpawnService _coinsSpawnService;

        [Inject]
        private void Construct(
            IPlayerDataService playerDataService,
            CoinsSpawnService coinsSpawnService)
        {
            _playerDataService = playerDataService;
            _coinsSpawnService = coinsSpawnService;
        }

        public override void OnNetworkSpawn()
        {
            HasCoin.AddTo(this);
            HasCoin.OnValueChanged += OnHasCoinChanged;
            _collider.OnTriggerEnterAsObservable()
                .Subscribe(HandleTriggerEnterServerSide)
                .AddTo(this);
        }

        public override void OnNetworkDespawn()
        {
            HasCoin.OnValueChanged -= OnHasCoinChanged;
        }

        private void OnHasCoinChanged(bool _, bool newValue)
        {
            ToggleCoinView(newValue);
        }

        private void ToggleCoinView(bool newValue)
        {
            if (newValue) _coinView.Spawn();
            else _coinView.Destroy();
        }

        private void HandleTriggerEnterServerSide(Collider other)
        {
            if (!IsServer)
                return;

            if (other.TryGetComponentInParent(out Coin resource))
                HandleCoinPickUp(resource);
            else if (other.TryGetComponentInParent(out Altar altar))
                HandleCoinHandOverToAltar(altar);
        }

        private void HandleCoinHandOverToAltar(Altar altar)
        {
            PlayerData playerData = _playerDataService[OwnerClientId];

            if (!HasCoin.Value || playerData.team.Value != altar.Team)
                return;

            HasCoin.Value = false;
            playerData.collectedCoins.Value++;
            _coinsSpawnService._coinsAmount--;
        }

        private void HandleCoinPickUp(Coin resource)
        {
            if (HasCoin.Value)
                return;

            HasCoin.Value = true;
            resource.GetComponentInChildren<CoinView>().Destroy();
            resource.NetworkObject.Despawn();
        }
    }
}