﻿using System.Linq;
using NaughtyAttributes;
using Networking;
using Networking.Teams;
using UniRx;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace DefaultNamespace
{
    public class CoinSpawner : MonoBehaviour
    {
        [SerializeField] private ETeam _team;
        [SerializeField] private int _spawnChanceWeight;
        [SerializeField] private Coin _prefab;
        [SerializeField] private Transform _checkCoinsPivot;
        [SerializeField] private float _checkCoinsRadius;

        private NetworkManager _networkManager;
        private NetworkObjectsManager _networkObjectsManager;
        private Collider[] _checkCoinsArray = new Collider[10];

        public bool CanSpawn { get; private set; }
        [ShowNativeProperty]
        public int SpawnChanceWeight => _spawnChanceWeight + AdditionalSpawnChanceWeight;
        [ShowNativeProperty]
        public int AdditionalSpawnChanceWeight { get; set; }
        public ETeam Team => _team;

        [Inject]
        private void Construct(
            NetworkManager networkManager,
            NetworkObjectsManager networkObjectsManager)
        {
            if (!networkManager.IsServer)
                return;

            _networkManager = networkManager;
            _networkObjectsManager = networkObjectsManager;
            Observable.EveryUpdate().Subscribe(_ => CheckIfCanSpawn()).AddTo(this);
        }

        public void SpawnServerSide()
        {
            if (!_networkManager.IsServer || !CanSpawn)
                return;

            _networkObjectsManager.Spawn(
                _prefab.GetComponent<NetworkObject>(),
                NetworkManager.ServerClientId,
                position: _checkCoinsPivot.position);
        }

        private void CheckIfCanSpawn()
        {
            Physics.OverlapSphereNonAlloc(_checkCoinsPivot.position, _checkCoinsRadius, _checkCoinsArray);
            CanSpawn = !_checkCoinsArray.Any(c => c != null && c.TryGetComponentInParent(out Coin _));
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (_checkCoinsPivot == null)
                return;
            
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(_checkCoinsPivot.position, _checkCoinsRadius);
        }
#endif
    }
}