﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.Configs;
using Networking;
using UniRx;
using Unity.Netcode;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

namespace DefaultNamespace
{
    public sealed class CoinsSpawnService : DisposableCollector
    {
        private readonly List<CoinSpawner> _spawners;
        private readonly IPlayerDataService _playerDataService;

        public int _coinsAmount;
        private readonly Random _random;
        private readonly AllInOneConfig _config;

        private float _spawnDelay;

        private CoinsSpawnService(
            NetworkManager networkManager,
            IPlayerDataService playerDataService,
            AllInOneConfigProvider allInOneConfigProvider)
        {
            if (!networkManager.IsServer)
                return;

            _random = new Random();
            _playerDataService = playerDataService;
            _spawners = Object.FindObjectsByType<CoinSpawner>(FindObjectsSortMode.None).ToList();
            _config = allInOneConfigProvider.GameConfig;

            Observable.EveryUpdate().Subscribe(_ => SpawnCoinIfNeeded()).AddTo(this);
        }

        private void SpawnCoinIfNeeded()
        {
            if (_spawners.Count < 1)
                return;
            int maxCoinsAmount = _playerDataService.PlayersData.Count switch
            {
                < 1 => 0,
                1 => _config.MaxCoinsOnePlayer,
                2 => _config.MaxCoinsTwoPlayers,
                3 => _config.MaxCoinsThreePlayers,
                _ => _config.MaxCoinsFourPlayers,
            };
            if (_coinsAmount < maxCoinsAmount)
            {
                if (_spawnDelay > 0)
                {
                    _spawnDelay -= Time.deltaTime;
                }
                else
                {
                    int spawnerIndex = GetSpawnerIndex();
                    if (_spawners.Count > 0 && _spawners[spawnerIndex].CanSpawn)
                    {
                        _spawners[spawnerIndex].SpawnServerSide();
                        foreach (var coinSpawner in _spawners.Where(s => s.Team == _spawners[spawnerIndex].Team))
                            coinSpawner.AdditionalSpawnChanceWeight = 0;
                        foreach (var coinSpawner in _spawners.Where(s => s.Team != _spawners[spawnerIndex].Team))
                            coinSpawner.AdditionalSpawnChanceWeight += _config.AdditionalWeightChangeStep;
                        _coinsAmount += 1;
                    }
                }
            }
            else
            {
                _spawnDelay = _config.CoinSpawnDelay;
            }
        }

        private int GetSpawnerIndex()
        {
            int totalWeight = _spawners.Sum(s => s.SpawnChanceWeight);
            if (totalWeight == 0)
                return _random.Next(0, _spawners.Count);
            float randomWeight = _random.Next(totalWeight);
            var spawnerIndex = _spawners.FindIndex(s => (randomWeight -= s.SpawnChanceWeight) < 0);
            if (spawnerIndex == -1)
            {
                Debug.LogWarning("[CoinsSpawnService] Spawner index is -1");
                spawnerIndex = 0;
            }
            return spawnerIndex;
        }
    }
}