﻿using UnityEngine;
using VContainer;

namespace DefaultNamespace
{
    public class CoinsSpawnServiceStarter : MonoBehaviour
    {
        private CoinsSpawnService _coinsSpawnService;

        [Inject]
        private void Construct(CoinsSpawnService coinsSpawnService)
        {
            _coinsSpawnService = coinsSpawnService;
        }
    }
}