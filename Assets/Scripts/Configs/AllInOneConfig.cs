﻿using UnityEngine;

namespace DefaultNamespace.Configs
{
    [CreateAssetMenu(menuName = "GameConfigs/AllInOneConfig", fileName = "AllInOneConfig.asset", order = 0)]
    public class AllInOneConfig : GameConfig
    {
        [field: Header("Coins"), SerializeField] public float CoinSpawnDelay { get; private set; }
        [field: SerializeField] public int MaxCoinsOnePlayer { get; private set; }
        [field: SerializeField] public int MaxCoinsTwoPlayers { get; private set; }
        [field: SerializeField] public int MaxCoinsThreePlayers { get; private set; }
        [field: SerializeField] public int MaxCoinsFourPlayers { get; private set; }
        [field: SerializeField] public int AdditionalWeightChangeStep { get; private set; }
        
        [field: Header("Button"), SerializeField] public float ButtonUnpressDelay { get; private set; }
        
        [field: Header("Player"), SerializeField] 
        public float DashPower { get; private set; }
        
        [field: SerializeField]
        public float PlayerSpeed { get; private set; }
    }

    public sealed class AllInOneConfigProvider : GameConfigProvider<AllInOneConfig>
    {
        public AllInOneConfigProvider() : base("GameConfigs/AllInOneConfig")
        {
        }
    }
}