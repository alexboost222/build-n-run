﻿namespace DefaultNamespace.Configs
{
    public abstract class GameConfigProvider<T> where T: GameConfig
    {
        protected GameConfigProvider(string path)
        {
            GameConfig = UnityEngine.Resources.Load<T>(path);
        }

        public T GameConfig { get; }
    }
}