using DefaultNamespace;
using Networking;
using VContainer;
using VContainer.Unity;

namespace DI
{
    public class NetworkingLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<NetworkObjectsManager>(Lifetime.Singleton);
            builder.RegisterComponentInHierarchy<GameFSM>();
            builder.Register<PlayerDataService>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.Register<CoinsSpawnService>(Lifetime.Singleton);
            builder.Register<RespawnPointsRegistry>(Lifetime.Singleton);
        }
    }
}