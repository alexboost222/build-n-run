using DefaultNamespace;
using Networking;
using SheeshController;
using Unity.Netcode;
using Unity.Netcode.Components;
using VContainer;
using VContainer.Unity;

namespace DI
{
    public class PlayerAvatarLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            var no = GetComponent<NetworkObject>();
            builder.RegisterInstance(no);
            builder.RegisterInstance(GetComponentInChildren<PlayerLifeController>());
            builder.RegisterInstance(GetComponentInChildren<PlayerRespawnController>());
            builder.RegisterInstance(GetComponentInChildren<NetworkTransform>());
            builder.RegisterInstance(GetComponentInChildren<PlayerTeleporter>());
            builder.RegisterInstance(GetComponentInChildren<SheeshController.SheeshController>());
            builder.RegisterInstance(GetComponentInChildren<AltarResourcePicker>());
            builder.RegisterInstance(GetComponentInChildren<GroundChecker>());

            if (no.IsOwner)
            {
                builder.Register<TMPHackControllerInputSetter>(Lifetime.Scoped).AsImplementedInterfaces();
            }
            else
            {
                builder.Register<EmptyController>(Lifetime.Scoped).AsImplementedInterfaces();
            }
        }
    }
}