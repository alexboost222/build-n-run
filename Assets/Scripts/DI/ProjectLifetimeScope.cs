using System;
using Cysharp.Threading.Tasks;
using DefaultNamespace;
using DefaultNamespace.Configs;
using Networking;
using Networking.Teams;
using Unity.Netcode;
using Unity.Services.Lobbies;
using UnityEngine;
using UnityEngine.Serialization;
using VContainer;
using VContainer.Unity;

namespace DI
{
    public class ProjectLifetimeScope : LifetimeScope
    {
        public NetworkManager prefab;
        private ApplicationFSM _applicationFSM;
        [SerializeField] private TeamColorsService teamColorsService;

        protected override void Configure(IContainerBuilder builder)
        {
            var networkManager = Instantiate(prefab);
            _applicationFSM = ApplicationFSM.CreateInstance(networkManager);

            builder.RegisterInstance(networkManager);
            builder.RegisterInstance(_applicationFSM);
            builder.RegisterInstance(teamColorsService);
            builder.Register<AllInOneConfigProvider>(Lifetime.Singleton);

            _applicationFSM.PushCommand(ApplicationFSM.GameCommands.Connect);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EditorPrefsSafeForBuild.SetBool("IsServer", true);
            prefab.Shutdown();
            
            _applicationFSM.OnDestroy();
        }
    }
}