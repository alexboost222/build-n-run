using Unity.Netcode;
using UnityEngine;

namespace DI
{
    public class RunNoScope : NetworkBehaviour
    {
        public override void OnNetworkSpawn()
        {
            GetComponent<PlayerAvatarLifetimeScope>().Build();
        }
    }
}