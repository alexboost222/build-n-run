﻿using System;
using System.Collections.Generic;

namespace DefaultNamespace
{
    public class DisposableCollector : IDisposable
    {
        private List<IDisposable> _container = new();

        public bool IsDisposed { get; private set; }

        public void AddDisposable(IDisposable disposable)
        {
            if (!_container.Contains(disposable))
                _container.Add(disposable);
        }

        public virtual void Dispose()
        {
            if (IsDisposed)
                return;
            IsDisposed = true;
            foreach (var disposable in _container)
                disposable.Dispose();
            _container = null;
        }
    }

    public static class DisposableExtensions
    {
        public static void AddTo(
            this IDisposable disposable,
            DisposableCollector disposableCollector)
        {
            disposableCollector.AddDisposable(disposable);
        }
    }
}