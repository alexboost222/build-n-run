﻿#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DefaultNamespace
{
    public static class EditorPrefsSafeForBuild
    {
        public static bool GetBool(string key)
        {
#if UNITY_EDITOR
            return EditorPrefs.GetBool(key);
#else
            return false;
#endif
        }

        public static void SetBool(string key, bool value)
        {
#if UNITY_EDITOR
            EditorPrefs.SetBool(key, value);
#endif
        }
    }
}