using UnityEngine;

public static class Extensions
{
    public static bool TryGetComponentInParent<T>(this Component component, out T componentInParent)
        where T : Component
    {
        componentInParent = component.GetComponentInParent<T>();
        return componentInParent != null;
    }

    public static bool TryGetComponentInChildren<T>(this Component component, out T componentInChildren, bool includeInactive = false)
        where T : Component
    {
        componentInChildren = component.GetComponentInChildren<T>(includeInactive);
        return componentInChildren != null;
    }
}