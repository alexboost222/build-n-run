using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DefaultNamespace;
using DI;
using FSM;
using Unity.Multiplayer.Playmode;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.Networking.Transport.Relay;
using Unity.Services.Authentication;
using Unity.Services.Core;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;
using UnityEngine;
using VContainer.Unity;

namespace Networking
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ApplicationFSM : ReflectionFSM<ApplicationFSM.GameStates, ApplicationFSM.GameCommands>
    {
        private const string LOG_TAG = "[ApplicationFSM]";
        private NetworkManager _manager;

        public enum GameStates
        {
            EnteredFromEditor,
            ConnectionReady,
            WaitingGameToStart,
        }

        public enum GameCommands
        {
            Connect,
            StartingWaitingForGame
        }

        public static ApplicationFSM CreateInstance(NetworkManager manager)
        {
            var instance = CreateInstance<ApplicationFSM>(GameStates.EnteredFromEditor);
            instance.LocalFSM
                .RegisterTransition(GameStates.ConnectionReady, GameStates.WaitingGameToStart,
                    GameCommands.StartingWaitingForGame);

            instance._manager = manager;
            return instance;
        }


        [Transition(GameStates.EnteredFromEditor, GameStates.ConnectionReady, GameCommands.Connect)]
        public void InitializeNetworking()
        {
            UniTask.Create(async () =>
            {
                await UniTask.Yield(); // TODO crutch because network manager not initializing properly somehow.
                
                
                
                
                Debug.Log($"{LOG_TAG} Signed in anonymously");
                
                var isServer = EditorPrefsSafeForBuild.GetBool("IsServer");
                if (!CurrentPlayer.ReadOnlyTags().Contains("MainPlayer"))
                    await StartClient();
                else if (isServer)
                    await StartHost();
                else
                    await StartClient();

                Debug.Log($"{LOG_TAG} Await network manager start");
                await UniTask.WaitUntil(() => (_manager.IsClient || _manager.IsHost) && _manager.CustomMessagingManager != null);
                Debug.Log($"{LOG_TAG} Network manager custom messaging manager set");

                PushCommand(GameCommands.StartingWaitingForGame);
                Debug.Log($"{LOG_TAG} Pushed StartingWaitingForGame command");
            });
        }


        private async UniTask StartClient()
        {
            if (_manager.GetComponent<UnityTransport>().Protocol == UnityTransport.ProtocolType.RelayUnityTransport)
            {
                await UnityServices.InitializeAsync();
                AuthenticationService.Instance.SwitchProfile("Client");
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            
                Lobby lobby = null;
                while (lobby == null)
                {
                    var lobbies = await Lobbies.Instance.QueryLobbiesAsync();
                    if (lobbies.Results.Count > 0 && lobbies.Results.First().Players.Count > 0)
                    {
                        await UniTask.Delay(TimeSpan.FromSeconds(2));
                        lobby = await Lobbies.Instance.JoinLobbyByIdAsync(lobbies.Results.First().Id);
                    }
                
                    Debug.Log($"{LOG_TAG} Lobby not found");
                    await UniTask.Delay(TimeSpan.FromSeconds(2));
                }
            
                Debug.Log($"{LOG_TAG} LobbyId: {lobby.Id}");
                var joinCode = "";
                while (string.IsNullOrEmpty(joinCode))
                {
                    lobby = await LobbyService.Instance.GetLobbyAsync(lobby.Id);
                    joinCode = lobby.Data?["RelayJoinCode"].Value;
                    Debug.Log($"{LOG_TAG} {joinCode}");

                    await UniTask.Delay(TimeSpan.FromSeconds(2));
                }
            
                var allocation = await RelayService.Instance.JoinAllocationAsync(joinCode: joinCode);
                _manager.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocation, "dtls"));
            }
            
            _manager.StartClient();
        }

        private async UniTask StartHost()
        {
            if (_manager.GetComponent<UnityTransport>().Protocol == UnityTransport.ProtocolType.RelayUnityTransport)
            {
                await UnityServices.InitializeAsync();
                AuthenticationService.Instance.SwitchProfile("Host");
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
            
                await UniTask.Delay(TimeSpan.FromSeconds(2));
            
                var lobbyIds = await LobbyService.Instance.GetJoinedLobbiesAsync();
                Debug.Log($"{LOG_TAG} LobbyIds: {string.Join(", ", lobbyIds)}");
            
                foreach (var id in lobbyIds)
                {
                    await LobbyService.Instance.DeleteLobbyAsync(id);
                    Debug.Log($"{LOG_TAG} Deleted lobby {id}");
                    await UniTask.Delay(TimeSpan.FromSeconds(2));
                }
            
                var lobby = await LobbyService.Instance.CreateLobbyAsync("Single Lobby", 4, new CreateLobbyOptions
                {
                    IsPrivate = false
                });
                Debug.Log($"{LOG_TAG} Created lobby {lobby.Id}");
            
                await UniTask.Delay(TimeSpan.FromSeconds(2));
            
                var allocation = await RelayService.Instance.CreateAllocationAsync(4);
                Debug.Log($"{LOG_TAG} Created allocation");
            
                _manager.GetComponent<UnityTransport>().SetRelayServerData(new RelayServerData(allocation, "dtls"));
                var joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);
                Debug.Log($"{LOG_TAG} Got join code");
            
                await UniTask.Delay(TimeSpan.FromSeconds(2));
            
                await LobbyService.Instance.UpdateLobbyAsync(lobby.Id, new UpdateLobbyOptions
                {
                    Data = new Dictionary<string, DataObject>
                    {
                        ["RelayJoinCode"] = new(DataObject.VisibilityOptions.Public, joinCode)
                    }
                });
                Debug.Log($"{LOG_TAG} updated lobby");
            
                Debug.Log(joinCode);
            }
            
            
            _manager.StartHost();
        }
        
        

        [OnEnter(GameStates.WaitingGameToStart)]
        public void OnEnterStartWaiting()
        {
            LifetimeScope.Find<NetworkingLifetimeScope>().Build();
        }

        public void OnDestroy()
        {
            // UniTask.Create(async () =>
            // {
            //     var lobbyIds = await LobbyService.Instance.GetJoinedLobbiesAsync();
            //     Debug.Log($"LobbyIds: {string.Join(", ", lobbyIds)}");
            //
            //     foreach (var id in lobbyIds)
            //     {
            //         await LobbyService.Instance.DeleteLobbyAsync(id);
            //         Debug.Log($"Deleted lobby {id}");
            //         await UniTask.Delay(TimeSpan.FromSeconds(2));
            //     }
            // }).Forget();
        }
    }
}