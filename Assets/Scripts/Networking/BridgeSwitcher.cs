﻿using DG.Tweening;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;

namespace Networking
{
    public class BridgeSwitcher : NetworkBehaviour
    {
        [SerializeField] private GroundButton _groundButton;
        [SerializeField] private NetworkTransform _bridgeNetworkTransform;
        [SerializeField] private Transform _pressedPivot;
        [SerializeField] private Transform _unpressedPivot;
        [SerializeField] private float _openCloseTime;

        public override void OnNetworkSpawn()
        {
            if (!IsServer)
                return;
            _groundButton.IsPressed.OnValueChanged += OnIsPressedValueChanged;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _groundButton.IsPressed.OnValueChanged -= OnIsPressedValueChanged;
        }

        private void OnIsPressedValueChanged(bool _, bool newValue)
        {
            _bridgeNetworkTransform.transform.DOKill();
            Vector3 target = newValue ? _pressedPivot.position : _unpressedPivot.position;
            _bridgeNetworkTransform.transform.DOMove(target, _openCloseTime);
        }
    }
}