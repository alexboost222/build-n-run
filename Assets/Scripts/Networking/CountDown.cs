using System;
using System.Collections;
using Unity.Netcode;
using UnityEngine;

namespace Networking
{
    public class CountDown : NetworkBehaviour
    {
        [SerializeField] private float maxTimer = 3f;

        public NetworkVariable<bool> isCounting = new();
        public NetworkVariable<float> timeLeft = new();
        private Coroutine _timerHandle;
        
        private Action _callback;
        public void StartCountDown(Action callback)
        {
            _callback = callback;
            StartCountDown(maxTimer);
        }

        private void StartCountDown(float time)
        {
            isCounting.Value = true;
            timeLeft.Value = time;
            _timerHandle = StartCoroutine(Timer());
        }

        private IEnumerator Timer()
        {
            while (timeLeft.Value > 0)
            {
                yield return null;
                timeLeft.Value -= Time.deltaTime;
            }

            _callback?.Invoke();
            StopCountDown();
        }

        public void StopCountDown()
        {
            isCounting.Value = false;
            if (_timerHandle != null) StopCoroutine(_timerHandle);
        }
        
    }
}