using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class DisableIfNotOwner : MonoBehaviour
    {
        [Inject]
        public void Construct(NetworkObject no)
        {
            gameObject.SetActive(no.IsOwner);
        }
    }
}