﻿using System;
using DefaultNamespace;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class DropCoinOnDeath : MonoBehaviour
    {
        [SerializeField] private Coin _coinPrefab;

        private NetworkManager _networkManager;
        private NetworkObjectsManager _networkObjectsManager;
        private AltarResourcePicker _altarResourcePicker;
        private GroundChecker _groundChecker;
        private PlayerLifeController _playerLifeController;

        [Inject]
        private void Construct(
            NetworkManager networkManager,
            NetworkObjectsManager networkObjectsManager,
            AltarResourcePicker altarResourcePicker,
            GroundChecker groundChecker,
            PlayerLifeController playerLifeController)
        {
            _networkManager = networkManager;
            _networkObjectsManager = networkObjectsManager;
            _altarResourcePicker = altarResourcePicker;
            _groundChecker = groundChecker;
            _playerLifeController = playerLifeController;

            if (!networkManager.IsServer)
                return;

            _playerLifeController.isAlive.OnValueChanged += OnIsAliveValueChanged;
        }

        private void OnDestroy()
        {
            if (_playerLifeController.isAlive != null)
                _playerLifeController.isAlive.OnValueChanged -= OnIsAliveValueChanged;
        }

        private void OnIsAliveValueChanged(bool _, bool newValue)
        {
            if (!newValue && _altarResourcePicker.HasCoin.Value && _groundChecker.LastGroundPoint != null)
            {
                _altarResourcePicker.HasCoin.Value = false;
                _networkObjectsManager.Spawn(
                    _coinPrefab.GetComponent<NetworkObject>(), 
                    NetworkManager.ServerClientId,
                    position: _groundChecker.LastGroundPoint.Value);
            }
        }
    }
}