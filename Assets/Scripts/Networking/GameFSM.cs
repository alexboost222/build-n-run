using System;
using FSM;
using UnityEngine;

namespace Networking
{
    public class GameFSM : NetworkingFSM<GameFSM.State, GameFSM.Command>
    {
        public enum State
        {
            WaitingToStart,
            Countdown,
            Game,
            EndScreen,
        }

        public enum Command
        {
            AllPlayerOnPlatform,
            SomeoneNotOnPlatform,
            TimerElapsed,
            GameOver,
            ResetGameToWaiting,
        }
        
        [SerializeField] private CountDown waitingToStart;
        [SerializeField] private CountDown gameTimer;

        private void Awake()
        {
            RegisterTransition(State.WaitingToStart, State.Countdown, Command.AllPlayerOnPlatform);
            RegisterTransition(State.Countdown, State.WaitingToStart, Command.SomeoneNotOnPlatform);
            RegisterTransition(State.Countdown, State.Game, Command.TimerElapsed);
            RegisterTransition(State.Game, State.EndScreen, Command.GameOver);
            RegisterTransition(State.EndScreen, State.WaitingToStart, Command.ResetGameToWaiting);

            RegisterOnEnter(State.Countdown, () => waitingToStart.StartCountDown(() => PushCommand(Command.TimerElapsed)));
            RegisterOnExit(State.Countdown, () => waitingToStart.StopCountDown());

            RegisterOnEnter(State.Game, () => gameTimer.StartCountDown(() => PushCommand(Command.GameOver)));
            RegisterOnExit(State.Game, () => gameTimer.StopCountDown());
        }
    }
}