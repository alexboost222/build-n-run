﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using DefaultNamespace.Configs;
using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class GroundButton : NetworkBehaviour
    {
        [SerializeField] private Collider _collider;

        public readonly NetworkVariable<bool> IsPressed = new();

        private readonly List<SheeshController.SheeshController> _controllersInTrigger = new();
        private CancellationTokenSource _unpressDelayTokenSource;
        private AllInOneConfig _config;

        [Inject]
        private void Construct(AllInOneConfigProvider configProvider)
        {
            _config = configProvider.GameConfig;
        }

        public override void OnNetworkSpawn()
        {
            IsPressed.AddTo(this);
            if (!IsServer)
                return;
            _collider.OnTriggerEnterAsObservable()
                .Subscribe(HandleTriggerEnter)
                .AddTo(this);
            _collider.OnTriggerExitAsObservable()
                .Subscribe(HandleTriggerExit)
                .AddTo(this);
        }

        private void HandleTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody == null ||
                !other.attachedRigidbody.TryGetComponentInChildren(out SheeshController.SheeshController sheeshController, true))
            {
                return;
            }

            if (!_controllersInTrigger.Contains(sheeshController))
            {
                _controllersInTrigger.Add(sheeshController);
                IsPressed.Value = true;
            }
        }

        private void HandleTriggerExit(Collider other)
        {
            if (other.attachedRigidbody == null ||
                !other.attachedRigidbody.TryGetComponentInChildren(out SheeshController.SheeshController sheeshController, true))
            {
                return;
            }

            _controllersInTrigger.Remove(sheeshController);
            if (_controllersInTrigger.Count < 1 && _unpressDelayTokenSource == null)
            {
                _unpressDelayTokenSource = new CancellationTokenSource();
                UniTask.Create(async () =>
                {
                    await UniTask.WaitForSeconds(
                            _config.ButtonUnpressDelay,
                            cancellationToken: _unpressDelayTokenSource.Token)
                        .SuppressCancellationThrow();
                    if (!_unpressDelayTokenSource.IsCancellationRequested)
                        IsPressed.Value = false;
                    _unpressDelayTokenSource.Dispose();
                    _unpressDelayTokenSource = null;
                });
            }
        }
    }
}