﻿using System;
using UnityEngine;

namespace Networking
{
    public class GroundButtonView : MonoBehaviour
    {
        [SerializeField] private GroundButton _groundButton;
        [SerializeField] private GameObject _pressedView;
        [SerializeField] private GameObject _unpressedView;

        private void OnIsPressedValueChanged(bool _, bool newValue)
        {
            _pressedView.SetActive(newValue);
            _unpressedView.SetActive(!newValue);
        }

        private void Awake()
        {
            _groundButton.IsPressed.OnValueChanged += OnIsPressedValueChanged;
        }

        private void OnDestroy()
        {
            _groundButton.IsPressed.OnValueChanged -= OnIsPressedValueChanged;
        }
    }
}