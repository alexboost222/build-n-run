﻿using NaughtyAttributes;
using UnityEngine;

namespace Networking
{
    public class GroundChecker : MonoBehaviour
    {
        [SerializeField] private LayerMask _groundLayerMask;

        [ShowNativeProperty]
        public Vector3? LastGroundPoint { get; private set; }

        private void Update()
        {
            if (Physics.Raycast(transform.position, -transform.up, out var hit, 5f, _groundLayerMask))
                LastGroundPoint = hit.point;
        }
    }
}