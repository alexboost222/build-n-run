﻿using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class LaserSwitcher : MonoBehaviour
    {
        [SerializeField] private GroundButton _groundButton;
        [SerializeField] private PlayerDeadZone _playerDeadZone;
        [SerializeField] private GameObject _view;
        private NetworkManager _networkManager;

        [Inject]
        private void Construct(NetworkManager networkManager)
        {
            _networkManager = networkManager;
            _groundButton.IsPressed.OnValueChanged += OnIsPressedValueChanged;
        }

        private void OnDestroy()
        {
            _groundButton.IsPressed.OnValueChanged -= OnIsPressedValueChanged;
        }

        private void OnIsPressedValueChanged(bool _, bool newValue)
        {
            if (_networkManager.IsServer)
                _playerDeadZone.Enabled = !newValue;
            _view.SetActive(!newValue);
        }
    }
}