using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

namespace Networking
{
    public class NetworkObjectsManager
    {
        [Serializable]
        private struct SerializableSpawnData : IEquatable<SerializableSpawnData>
        {
            public int networkObjectId;
            public Vector3 position;
            public Quaternion rotation;
            public ulong owner;
            public bool isPlayerObject;

            public bool Equals(SerializableSpawnData other)
            {
                return networkObjectId == other.networkObjectId && position.Equals(other.position) && rotation.Equals(other.rotation) && owner == other.owner && isPlayerObject == other.isPlayerObject;
            }

            public override bool Equals(object obj)
            {
                return obj is SerializableSpawnData other && Equals(other);
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(networkObjectId, position, rotation, owner, isPlayerObject);
            }

            public static bool operator ==(SerializableSpawnData left, SerializableSpawnData right)
            {
                return left.Equals(right);
            }

            public static bool operator !=(SerializableSpawnData left, SerializableSpawnData right)
            {
                return !left.Equals(right);
            }
        }
        
        private const string MESSAGE_NAME = "SpawnNetworkObject";


        private List<NetworkObject> AllObjects { get; }

        private readonly NetworkManager _manager;

        
        public NetworkObjectsManager(NetworkManager manager)
        {
            AllObjects = Resources.LoadAll<GameObject>("NetworkObjects")
                .Select(obj => obj.TryGetComponent<NetworkObject>(out var no) ? no : null)
                .Where(no => no != null)
                .OrderBy(o => o.name)
                .ToList();

            _manager = manager;

            foreach (var no in AllObjects)
                _manager.AddNetworkPrefab(no.gameObject);
            
            _manager.CustomMessagingManager.RegisterNamedMessageHandler(MESSAGE_NAME, ReceiveSpawnMessage);
        }

        private void ReceiveSpawnMessage(ulong senderClientId, FastBufferReader messagePayload)
        {
            messagePayload.ReadValueSafe(out ForceNetworkSerializeByMemcpy<SerializableSpawnData> spawnDataReader);
            var spawnData = spawnDataReader.Value;

            AllObjects[spawnData.networkObjectId].InstantiateAndSpawn(
                networkManager: _manager,
                ownerClientId: spawnData.owner,
                destroyWithScene: true,
                isPlayerObject: spawnData.isPlayerObject,
                position: spawnData.position,
                rotation: spawnData.rotation);
        }

        public void Spawn(NetworkObject go, ulong owner, bool isPlayerObject = false, Vector3 position = default, Quaternion rotation = default)
        {
            var messageContent = new ForceNetworkSerializeByMemcpy<SerializableSpawnData>(new SerializableSpawnData
            {
                networkObjectId = AllObjects.IndexOf(go),
                owner = owner,
                isPlayerObject = isPlayerObject,
                position = position,
                rotation = rotation,
            });
            
            using var writer = new FastBufferWriter(1100, Allocator.Temp);
            writer.WriteValueSafe(messageContent);
            
            _manager.CustomMessagingManager.SendNamedMessage(MESSAGE_NAME, NetworkManager.ServerClientId, writer);
        }
        

        public void Dispose()
        {
            _manager.CustomMessagingManager.UnregisterNamedMessageHandler(MESSAGE_NAME);
        }
    }
}