using Networking.Teams;
using Unity.Netcode;

namespace Networking
{
    public class PlayerData : NetworkBehaviour
    {
        public ulong ClientId => OwnerClientId;
        public NetworkVariable<ETeam> team = new();
        public NetworkVariable<int> collectedCoins = new();
    }
}