using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using UniRx;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Networking
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class PlayerDataService : IPlayerDataService
    {
        public IReadOnlyReactiveCollection<PlayerData> PlayersData => _playersData;

        public readonly ReactiveCollection<PlayerData> _playersData = new();

        public PlayerDataService()
        {
            UniTask.Create(async () =>
            {
                while (Application.isPlaying)
                {
                    await UniTask.Delay(TimeSpan.FromSeconds(0.5f));
                    var playerDataInScene = Object.FindObjectsByType<PlayerData>(FindObjectsSortMode.None);
                    
                    foreach (var playerDataOnScene in playerDataInScene)
                    {
                        if (!_playersData.Contains(playerDataOnScene))
                            _playersData.Add(playerDataOnScene);
                    }
                    
                    foreach (var playerData in _playersData.ToList())
                    {
                        if (!playerDataInScene.Contains(playerData))
                            _playersData.Remove(playerData);
                    }
                }
            });
        }
        
        public PlayerData this[ulong clientId] => 
            Object
                .FindObjectsByType<PlayerData>(FindObjectsSortMode.None)
                .FirstOrDefault(x => x.ClientId == clientId);
    }

    public interface IPlayerDataService
    {
        public IReadOnlyReactiveCollection<PlayerData> PlayersData { get; }
        public abstract PlayerData this[ulong clientId] { get; }
    }
}