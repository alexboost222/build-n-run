﻿using NaughtyAttributes;
using UnityEngine;

namespace Networking
{
    public class PlayerDeadTester : MonoBehaviour
    {
        [Button]
        public void TestKill()
        {
            GetComponent<PlayerLifeController>().KillServerSide();
        }

        [Button]
        public void TestRevive()
        {
            GetComponent<PlayerLifeController>().ReviveServerSide();
        }
    }
}