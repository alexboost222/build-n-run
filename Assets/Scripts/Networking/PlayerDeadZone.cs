﻿using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class PlayerDeadZone : MonoBehaviour
    {
        [SerializeField] private Collider _collider;

        public bool Enabled { get; set; } = true;

        [Inject]
        private void Construct(NetworkManager networkManager)
        {
            if (!networkManager.IsServer)
                return;

            _collider.OnTriggerEnterAsObservable().Subscribe(HandleTriggerEnter).AddTo(this);
        }

        private void HandleTriggerEnter(Collider other)
        {
            if (!Enabled)
                return;

            if (!other.TryGetComponentInParent(out PlayerLifeController playerLifeController))
                return;

            playerLifeController.KillServerSide();
        }
    }
}