﻿using UniRx;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class PlayerFallChecker : MonoBehaviour
    {
        [SerializeField] private float _lowestAliveY;
        private PlayerLifeController _playerLifeController;
        private NetworkManager _networkManager;

        [Inject]
        private void Construct(
            PlayerLifeController playerLifeController,
            NetworkManager networkManager)
        {
            _playerLifeController = playerLifeController;
            _networkManager = networkManager;
            if (!_networkManager.IsServer)
                return;
            Observable.EveryUpdate().Subscribe(KillIfFallen).AddTo(this);
        }

        private void KillIfFallen(long _)
        {
            if (!_playerLifeController.isAlive.Value)
                return;
            if (_playerLifeController.GetComponentInParent<Rigidbody>().transform.position.y >= _lowestAliveY)
                return;
            _playerLifeController.KillServerSide();
        }
    }
}