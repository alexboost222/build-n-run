﻿using Unity.Netcode;
using VContainer;

namespace Networking
{
    public class PlayerLifeController : NetworkBehaviour
    {
        public NetworkVariable<bool> isAlive = new(true);
        private SheeshController.SheeshController _sheeshController;

        [Inject]
        private void Construct(
            SheeshController.SheeshController sheeshController)
        {
            _sheeshController = sheeshController;
        }

        public override void OnNetworkSpawn()
        {
            if (!IsServer)
                return;

            isAlive.OnValueChanged += OnIsAliveValueChanged;
        }

        public void KillServerSide()
        {
            if (!IsServer || !isAlive.Value)
                return;

            isAlive.Value = false;
        }

        public void ReviveServerSide()
        {
            if (!IsServer || isAlive.Value)
                return;

            isAlive.Value = true;
        }

        private void OnIsAliveValueChanged(bool previousValue, bool newValue)
        {
            _sheeshController.SetActiveClientRpc(newValue);
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            if (isAlive != null)
            {
                isAlive.OnValueChanged -= OnIsAliveValueChanged;
                isAlive.Dispose();
                isAlive = null;
            }
        }
    }
}