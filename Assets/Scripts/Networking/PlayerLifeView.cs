﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace Networking
{
    public class PlayerLifeView : MonoBehaviour
    {
        [SerializeField] private GameObject _deadView;

        private PlayerLifeController _controller;

        private IEnumerator Start()
        {
            _controller = null;
            while (_controller == null)
            {
                _controller = FindObjectsByType<PlayerLifeController>
                    (FindObjectsInactive.Include, FindObjectsSortMode.None)
                    .FirstOrDefault(p => p.IsOwner);
                yield return null;
            }
            if (_controller.isAlive != null)
            {
                OnIsAliveValueChanged(false, _controller.isAlive.Value);
                _controller.isAlive.OnValueChanged += OnIsAliveValueChanged;
            }
        }

        private void OnDestroy()
        {
            if (_controller.isAlive != null)
                _controller.isAlive.OnValueChanged -= OnIsAliveValueChanged;
        }

        private void OnIsAliveValueChanged(bool previousValue, bool newValue)
        {
            _deadView.SetActive(!newValue);
        }
    }
}