﻿using Cysharp.Threading.Tasks;
using Networking.Teams;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class PlayerRespawnController : NetworkBehaviour
    {
        [SerializeField] private float _respawnTime;

        private RespawnPointsRegistry _respawnPointsRegistry;
        private IPlayerDataService _playerDataService;
        private PlayerLifeController _playerLifeController;
        private PlayerTeleporter _playerTeleporter;

        [Inject]
        private void Construct(
            RespawnPointsRegistry respawnPointsRegistry,
            IPlayerDataService playerDataService,
            PlayerLifeController playerLifeController,
            PlayerTeleporter playerTeleporter)
        {
            _playerDataService = playerDataService;
            _respawnPointsRegistry = respawnPointsRegistry;
            _playerLifeController = playerLifeController;
            _playerTeleporter = playerTeleporter;

            _playerLifeController.isAlive.OnValueChanged += OnIsAliveValueChanged;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            if (_playerLifeController.isAlive != null)
                _playerLifeController.isAlive.OnValueChanged -= OnIsAliveValueChanged;
        }

        private void OnIsAliveValueChanged(bool previousValue, bool newValue)
        {
            if (!IsServer)
                return;
            if (!newValue)
            {
                UniTask.Create(async () =>
                {
                    await UniTask.WaitForSeconds(_respawnTime);
                    ETeam team = _playerDataService[OwnerClientId].team.Value;
                    Vector3 respawnPoint = _respawnPointsRegistry.GetRespawnPointByTeam(team);
                    _playerTeleporter.TeleportPlayerClientRpc(respawnPoint);
                    _playerLifeController.ReviveServerSide();
                });
            }
        }
    }
}