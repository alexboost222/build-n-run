using System.Linq;
using UniRx;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class PlayerSpawner : MonoBehaviour
    {
        public NetworkObject playerPrefab;
        
        [Inject]
        public void Construct(
            NetworkManager manager, 
            NetworkObjectsManager spawnManager)
        {
            if (!manager.IsHost) return;
            
            manager.OnConnectionEvent += (m, data) =>
            {
                if (data.EventType != ConnectionEvent.ClientConnected) return;
                
                SendSpawnRequest(data.ClientId);
            };
            SendSpawnRequest(manager.LocalClientId);
            
            return;
            
            void SendSpawnRequest(ulong id) => spawnManager.Spawn(
                go: playerPrefab, 
                owner: id, 
                isPlayerObject: true, 
                position: transform.position);
        }
    }
}