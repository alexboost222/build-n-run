﻿using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class PlayerTeleporter : NetworkBehaviour
    {
        private NetworkTransform _networkTransform;

        [Inject]
        private void Construct(NetworkTransform networkTransform)
        {
            _networkTransform = networkTransform;
        }

        [ClientRpc]
        public void TeleportPlayerClientRpc(Vector3 position)
        {
            if (!IsOwner)
                return;
            _networkTransform.Teleport(position, Quaternion.identity, Vector3.one);
        }
    }
}