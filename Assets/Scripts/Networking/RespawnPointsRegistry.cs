﻿using System.Collections.Generic;
using Networking.Teams;
using UnityEngine;
using VContainer;

namespace Networking
{
    public class RespawnPointsRegistry
    {
        private Dictionary<ETeam, TeleportOnGameStart> _respawnPointsByTeam = new();

        [Inject]
        private void Construct()
        {
            var spawnPoints = UnityEngine.Object
                .FindObjectsByType<TeleportOnGameStart>(
                FindObjectsInactive.Include,
                FindObjectsSortMode.None);
            foreach (var spawnPoint in spawnPoints)
            {
                _respawnPointsByTeam.Add(spawnPoint.Team, spawnPoint);
            }
        }

        public Vector3 GetRespawnPointByTeam(ETeam team) => _respawnPointsByTeam[team].transform.position;
    }
}