namespace Networking.Teams
{
    public enum ETeam
    {
        Unknown = 0,
        Team1 = 1,
        Team2 = 2,
        Team3 = 3,
        Team4 = 4,
    }
}