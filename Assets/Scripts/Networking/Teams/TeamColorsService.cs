using System;
using System.Collections.Generic;
using UnityEngine;

namespace Networking.Teams
{
    [CreateAssetMenu]
    public class TeamColorsService : ScriptableObject
    {
        [Serializable]
        public struct TeamData
        {
            public Color mainTeamColor;
            public Material teamMaterial;
        }

        public DictionaryETeamTeamData data;
        
        public readonly Dictionary<ETeam, Color> Colors = new()
        {
            [ETeam.Team1] = new Color32(0xFF, 0x00, 0x00, 0xFF), // FF0000
            [ETeam.Team2] = new Color32(0x0B, 0xD1, 0x38, 0xFF), // 0BD138
            [ETeam.Team3] = new Color32(0x00, 0x60, 0xFF, 0xFF), // 0060FF
            [ETeam.Team4] = new Color32(0xFF, 0xFB, 0x00, 0xFF), // FFFB00
        };


        [Serializable]
        public class DictionaryETeamTeamData : SerializableDictionary<ETeam, TeamData> { }
    }
}