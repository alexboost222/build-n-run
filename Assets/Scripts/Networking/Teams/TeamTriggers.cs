using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace Networking.Teams
{
    public class TeamTriggers : MonoBehaviour
    {
        public TeamColliderDictionary teamZones;

        private readonly ReactiveCollection<NetworkObject> _trackedPlayers = new();
        
        [Inject]
        public void Construct(NetworkManager manager, 
            GameFSM gameFSM, 
            IPlayerDataService playerDataService, 
            TeamColorsService teamColorsService)
        {
            foreach (var team in teamZones.Keys)
            {
                if (!teamColorsService.Colors.TryGetValue(team, out var color)) continue;
                
                foreach (var image in teamZones[team].GetComponentsInChildren<Image>())
                {
                    image.color = color;    
                }
            }
            
            _trackedPlayers
                .ObserveCountChanged()
                .Select(_ => _trackedPlayers.Select(p => playerDataService[p.OwnerClientId].team.Value))
                .Subscribe(teams =>
                {
                    var t = new Dictionary<ETeam, int>();
                    foreach (var team in teams) t[team] = t.GetValueOrDefault(team, 0) + 1;
                    
                    foreach (var team in teamZones.Keys)
                    {
                        var slider = teamZones[team].GetComponentInChildren<Slider>();
                        if (slider == null) continue;
                        slider.value = (float) t.GetValueOrDefault(team, 0) / 4;
                    }
                }).AddTo(this);
            
            
            
            
            foreach (var team in teamZones.Keys)
            {
                teamZones[team]
                    .OnTriggerEnterAsObservable()
                    .Where(_ => gameFSM.CurrentState.Value != GameFSM.State.Game)
                    .Subscribe(col =>
                    {
                        if (!col.TryGetComponentInParent<NetworkObject>(out var no) ||
                            _trackedPlayers.Contains(no)) return;
                        
                        if (manager.IsServer) playerDataService[no.OwnerClientId].team.Value = team;
                        _trackedPlayers.Add(no);
                    })
                    .AddTo(this);
                
                teamZones[team]
                    .OnTriggerExitAsObservable()
                    .Where(_ => gameFSM.CurrentState.Value != GameFSM.State.Game)
                    .Subscribe(col =>
                    {
                        if (!col.TryGetComponentInParent<NetworkObject>(out var no) ||
                            !_trackedPlayers.Contains(no)) return;
                        
                        if (manager.IsServer) playerDataService[no.OwnerClientId].team.Value = ETeam.Unknown;
                        _trackedPlayers.Remove(no);
                    })
                    .AddTo(this);
            }

            _trackedPlayers
                .ObserveCountChanged()
                .Where(_ => manager.IsServer)
                .Subscribe(count => gameFSM.PushCommand(count == manager.ConnectedClientsList.Count
                    ? GameFSM.Command.AllPlayerOnPlatform
                    : GameFSM.Command.SomeoneNotOnPlatform))
                .AddTo(this);
        }



        [Serializable]
        public class TeamColliderDictionary : SerializableDictionary<ETeam, Collider> { }
    }
}