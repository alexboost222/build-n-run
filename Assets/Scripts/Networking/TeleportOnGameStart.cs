using System.Linq;
using DefaultNamespace;
using Networking.Teams;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.Serialization;
using VContainer;

namespace Networking
{
    public class TeleportOnGameStart : MonoBehaviour
    {
        [SerializeField] private GameFSM.State stateWhenTeleport;
        [SerializeField] private ETeam team;
        
        private IPlayerDataService _playerDataService;

        public ETeam Team => team;

        [Inject]
        public void Construct(GameFSM gameFSM, IPlayerDataService playerDataService)
        {
            _playerDataService = playerDataService;
            gameFSM.CurrentState.OnValueChanged += (_, state) =>
            {
                if (state == stateWhenTeleport) TeleportPlayersToPoint();
            };
        }

        private void TeleportPlayersToPoint()
        {
            var playerTransforms = FindObjectsByType<SheeshController.SheeshController>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .Select(c => c.GetComponentInParent<NetworkTransform>())
                .Where(nt =>
                {
                    var playerTeam = _playerDataService[nt.GetComponent<NetworkObject>().OwnerClientId].team.Value;
                    if (team == ETeam.Unknown) return true;
                    return playerTeam == team;
                })
                .ToList();

            for (var i = 0; i < playerTransforms.Count; i++)
            {
                var playerTransform = playerTransforms[i];
                if (!playerTransform.IsOwner) continue;
                var teleportTo = transform.position + transform.up * i * 2f;
                Debug.Log($"[TeleportOnGameStart] {teleportTo}");
                playerTransform.Teleport(teleportTo, Quaternion.identity, Vector3.one);
            }
        }
    }
}