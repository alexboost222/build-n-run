﻿using System;
using UnityEngine;

namespace SheeshController
{
    public interface ISheeshControllerInput
    {
        Vector2 HorizontalInput { get; }
        // event Action DashInput;
        bool DashInputHappened { get; }
    }
}