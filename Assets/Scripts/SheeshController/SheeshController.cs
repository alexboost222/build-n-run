using System;
using System.Collections;
using System.Linq;
using DefaultNamespace.Configs;
using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace SheeshController
{
    public class SheeshController : NetworkBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;

        [SerializeField] private AnimationCurve dashAnim;
        [SerializeField] private float dashDuration;
        [SerializeField] private float dashCooldown;
        [SerializeField] private float lerpRotation;

        [SerializeField] private Collider collisionCollider;
        
        public NetworkVariable<bool> isInDash = new(writePerm: NetworkVariableWritePermission.Owner);
        private bool _isInDashPredicted;
        [SerializeField] private bool isDashInCooldown;
        
        private ISheeshControllerInput _input;
        private AllInOneConfigProvider _configProvider;

        [Inject]
        private void Construct(ISheeshControllerInput input, AllInOneConfigProvider configProvider)
        {
            _configProvider = configProvider;
            _input = input;
            
            if (NetworkObject.IsOwner)
                _rigidbody
                    .OnCollisionEnterAsObservable()
                    .Where(col => col.rigidbody != null)
                    .Where(col => col.rigidbody.GetComponentInChildren<SheeshController>(includeInactive: true) != null)
                    .Select(col => col.rigidbody.GetComponent<NetworkObject>())
                    .Where(_ => _isInDashPredicted)
                    .Buffer(TimeSpan.FromSeconds(dashDuration / 4))
                    .Subscribe(other =>
                    {
                        foreach (var no in other.Where(o => o != null).Distinct()) 
                            PushPlayerServerRpc(no, no.transform.position - transform.position);
                    })
                    .AddTo(this);
        }

        [ServerRpc(RequireOwnership = false)]
        public void PushPlayerServerRpc(NetworkObjectReference networkObjectReference, Vector3 direction)
        {
            PushPlayerClientRpc(networkObjectReference, direction);
        }

        [ClientRpc(RequireOwnership = false)]
        public void PushPlayerClientRpc(NetworkObjectReference networkObjectReference, Vector3 direction)
        {
            if (!networkObjectReference.TryGet(out var networkObject)) Debug.Log("error");
            networkObject.GetComponent<Rigidbody>().AddForce(direction.normalized * 20, ForceMode.Impulse);
        }
        

        [ClientRpc]
        public void SetActiveClientRpc(bool isActive)
        {
            enabled = isActive;
            _rigidbody.velocity = Vector3.zero;
        }

        private void Update()
        {
            Debug.Log(transform.position);
            DoRotate(_input.HorizontalInput);
            if (_input.DashInputHappened)
                DoDash();
            DoHorizontalMovement(_input.HorizontalInput);
        }

        private void DoRotate(Vector2 input)
        {
            if (input.magnitude > 0.1f)
            {
                var rot = new Vector3(input.x, 0f, input.y);
                _rigidbody.transform.forward = Vector3.Lerp(_rigidbody.transform.forward, rot, lerpRotation * Time.deltaTime * 100 * (Vector3.Angle(_rigidbody.transform.forward, rot) > 170 ? 3 : 1));
            }
            
        }

        private void DoDash()
        {
            if (!isDashInCooldown)
            {
                _isInDashPredicted = true;
                StartCoroutine(DashRoutine());
            }
        }


        private IEnumerator DashRoutine()
        {
            var direction = new Vector3(_input.HorizontalInput.x, 0, _input.HorizontalInput.y);

            isDashInCooldown = true;
            isInDash.Value = true;
            
            _isInDashPredicted = true;
            
            var timer = dashDuration;
            while (timer > 0)
            {
                _rigidbody.AddForce(direction.normalized * _configProvider.GameConfig.DashPower * dashAnim.Evaluate(1 - timer / dashDuration), ForceMode.VelocityChange);
                timer -= Time.deltaTime;
                yield return null;
            }
            
            _isInDashPredicted = false;
            isInDash.Value = false;

            timer = dashCooldown - dashDuration;
            while (timer > 0)
            {
                timer -= Time.deltaTime;
                yield return null;
            }

            isDashInCooldown = false;
        }

        private void DoHorizontalMovement(Vector2 input)
        {
            Vector2 horizontalVelocity = Vector2.ClampMagnitude(input * _configProvider.GameConfig.PlayerSpeed, _configProvider.GameConfig.PlayerSpeed);
            float xComponent = Mathf.Abs(_rigidbody.velocity.x) > Mathf.Abs(horizontalVelocity.x)
                ? _rigidbody.velocity.x
                : horizontalVelocity.x;
            float zComponent = Mathf.Abs(_rigidbody.velocity.z) > Mathf.Abs(horizontalVelocity.y)
                ? _rigidbody.velocity.z
                : horizontalVelocity.y;
            _rigidbody.velocity = new Vector3(
                xComponent,
                _rigidbody.velocity.y,
                zComponent);
        }
    }
}

