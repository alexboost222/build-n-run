﻿using System;
using DefaultNamespace;
using UniRx;
using UnityEngine;

namespace SheeshController
{
    
    public class TMPHackControllerInputSetter : DisposableCollector, ISheeshControllerInput
    {
        public Vector2 HorizontalInput => new(
            Input.GetAxisRaw("Horizontal"), 
            Input.GetAxisRaw("Vertical"));

        public event Action DashInput;
        public bool DashInputHappened { get; private set; }

        public TMPHackControllerInputSetter()
        {
            Observable.EveryUpdate()
                .Select(_ => Input.GetButtonDown("Jump"))
                .Subscribe(j =>
                {
                    DashInputHappened = j;
                    if (j) DashInput?.Invoke();
                })
                .AddTo(this);
        }
    }

    public class EmptyController : ISheeshControllerInput
    {
        public Vector2 HorizontalInput { get; }
        // public event Action DashInput;
        public bool DashInputHappened { get; }
    }
}