using TMPro;
using UnityEngine;

namespace Networking
{
    public class CountDownView : MonoBehaviour
    {
        [SerializeField] private CountDown countDown;
        [SerializeField] private TMP_Text text;
        
        private void Start()
        {
            text.gameObject.SetActive(false);
            countDown.isCounting.OnValueChanged += (_, newValue) => text.gameObject.SetActive(newValue);
            countDown.timeLeft.OnValueChanged += (_, newValue) => text.text = $"{newValue:F1}";
        }


    }
}