using System.Collections.Generic;
using System.Linq;
using Networking;
using Networking.Teams;
using TMPro;
using UniRx;
using UniRx.Triggers;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace UI
{
    public class EndScreen : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;

        [SerializeField] private TMP_Text playerMessage;
        [SerializeField] private Button btn;
        
        [Inject]
        public void Construct(GameFSM fsm, NetworkManager manager, IPlayerDataService playerDataService)
        {
            canvas.enabled = false;
            btn.OnPointerClickAsObservable().Subscribe(_ => fsm.PushCommand(GameFSM.Command.ResetGameToWaiting)).AddTo(this);

            fsm.CurrentState.OnValueChanged += (_, state) =>
            {
                if (state == GameFSM.State.EndScreen)
                {
                    UpdateText();
                    canvas.enabled = true;
                    
                    foreach (var pd in playerDataService.PlayersData) pd.collectedCoins.Value = 0;
                }
                else
                    canvas.enabled = false;
            };
            return;

            void UpdateText()
            {
                var data = new Dictionary<ETeam, int>();

                foreach (var pd in playerDataService.PlayersData)
                {
                    if (data.ContainsKey(pd.team.Value))
                        data[pd.team.Value] += pd.collectedCoins.Value;
                    else
                        data.Add(pd.team.Value, pd.collectedCoins.Value);
                }
                
                playerMessage.text = string.Join("\n", data.Select(kvp => $"{kvp.Key}: {kvp.Value}"));
                btn.gameObject.SetActive(manager.IsServer);
                
            }
        }

        
    }
}