using System.Linq;
using Networking;
using Networking.Teams;
using TMPro;
using UniRx;
using UnityEngine;

namespace UI
{
    public class TeamView : MonoBehaviour
    {
        [SerializeField] private TMP_Text teamText;
        [SerializeField] private TMP_Text count;
        private ETeam _myTeam;

        public TeamView Construct(ETeam team, IPlayerDataService playerDataService)
        {
            _myTeam = team;
            teamText.text = team.ToString();
            
            gameObject.SetActive(true);
            
            foreach (var playerData in playerDataService.PlayersData)
                playerData.collectedCoins.OnValueChanged += (_, _) => RecalculateTeamPoints(playerDataService);
            
            playerDataService.PlayersData.ObserveAdd()
                .Subscribe(e => e.Value.collectedCoins.OnValueChanged += (_, _) => RecalculateTeamPoints(playerDataService))
                .AddTo(this);
            
            RecalculateTeamPoints(playerDataService);

            return this;
        }


        private void RecalculateTeamPoints(IPlayerDataService playerDataService)
        {
            count.text = playerDataService.PlayersData.Sum(p => p.team.Value == _myTeam ? p.collectedCoins.Value : 0).ToString();
        }
    }
}