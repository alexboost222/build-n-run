using System;
using System.Collections.Generic;
using System.Linq;
using Networking;
using Networking.Teams;
using UniRx;
using UnityEngine;
using VContainer;

namespace UI
{
    public class TeamsListView : MonoBehaviour
    {
        [SerializeField] private TeamView prefab;
        private readonly List<TeamView> _teamViews = new();
        private IPlayerDataService _playerDataService;

        private void Awake()
        {
            prefab.gameObject.SetActive(false);
        }

        [Inject]
        public void Construct(IPlayerDataService playerDataService)
        {
            _playerDataService = playerDataService;
            
            foreach (var playerData in playerDataService.PlayersData)
                playerData.team.OnValueChanged += (_, _) => RebuildList();
            
            playerDataService.PlayersData.ObserveAdd()
                .Subscribe(e => e.Value.team.OnValueChanged += (_, _) => RebuildList())
                .AddTo(this);
        }

        private void RebuildList()
        {
            var teams = new HashSet<ETeam>();
            
            foreach (var playerData in _playerDataService.PlayersData) teams.Add(playerData.team.Value);

            foreach (var view in _teamViews.ToList()) Destroy(view.gameObject);
            _teamViews.Clear();

            foreach (var team in teams) 
                _teamViews.Add(Instantiate(prefab, prefab.transform.parent).Construct(team, _playerDataService));
        }
    }
}