using System;
using System.Linq;
using SheeshController;
using UniRx;
using UnityEngine;
using UnityEngine.Serialization;
using VContainer;

namespace Views
{
    public class AvatarAnimatorController : MonoBehaviour
    {
        private static readonly int Speed = Animator.StringToHash("Speed");
        
        [SerializeField] private Animator animator;
        [SerializeField] private float maxSpeed;

        [Range(0, 1)]
        [SerializeField] private float lerpSpeed;

        [Inject]
        private ISheeshControllerInput _input;
        
        public void Start()
        {
            Observable
                .EveryUpdate()
                .Select(_ => _input.HorizontalInput.sqrMagnitude)
                .Subscribe(mag => animator.SetFloat(Speed,  Mathf.Lerp(mag / maxSpeed, animator.GetFloat(Speed), lerpSpeed)))
                .AddTo(this);
        }
    }
}