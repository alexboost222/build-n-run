using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace Views
{
    public class CoinView : MonoBehaviour
    {
        public Transform model;

        [SerializeField] private float spawnDuration;
        [SerializeField] private float flowDistance;
        [SerializeField] private ParticleSystem ps;

        [SerializeField] private Ease ease;

        [SerializeField] private bool autoSpawn;
        private Sequence _seq;

        private void Awake()
        {
            model.localScale = Vector3.zero;

            if (autoSpawn)
            {
                Spawn();
            }
        }

        public void Spawn()
        {
            model.DOScale(Vector3.one, spawnDuration).SetEase(Ease.OutBounce);

            _seq = DOTween.Sequence()
                .Append(model.DOLocalMoveY(flowDistance, 1).SetEase(ease))
                .Append(model.DOLocalMoveY(0, 1).SetEase(ease))
                .SetLoops(-1)
                .Play();
        }

        private void OnDestroy()
        {
            model.DOKill();
            _seq?.Kill();
        }

        public void Destroy()
        {
            ps.transform.parent = null;
            ps.Play();
            model.DOScale(Vector3.zero, 0);
        }
    }
}