using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Views
{
    public class DashView : MonoBehaviour
    {
        [SerializeField] private SheeshController.SheeshController controller;
        [SerializeField] private ParticleSystem ps;

        private void Start()
        {
            controller.isInDash.OnValueChanged += (_, value) =>
            {
                if (value)
                {
                    ps.Play();
                }
            };
        }
    }
}