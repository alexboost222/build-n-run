using System;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

namespace Views
{
    public class HatsView : NetworkBehaviour
    {
        public GameObject[] hats;
        
        public NetworkVariable<int> index = new(writePerm: NetworkVariableWritePermission.Owner);

        private void Start()
        {
            index.OnValueChanged += (value, newValue) =>
            {
                hats[value % hats.Length].SetActive(false);
                hats[newValue % hats.Length].SetActive(true);
            };

            foreach (var hat in hats) hat.SetActive(false);
            hats[0].SetActive(true);
        }

        public void Update()
        {
            if (!IsOwner) return;
            if (Input.GetKeyDown(KeyCode.P)) index.Value++;
        }
    }
}