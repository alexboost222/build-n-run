using Networking;
using Networking.Teams;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Views
{
    public class TeamBodyView : NetworkBehaviour
    {
        [SerializeField] private new Renderer renderer;
        
        [Inject] 
        private TeamColorsService _colorsService;
        
        [Inject] 
        private IPlayerDataService _playerDataService;

        
        public override void OnNetworkSpawn()
        {
            _playerDataService[OwnerClientId].team.OnValueChanged += (_, team) =>
            {
                renderer.material = _colorsService.data[team].teamMaterial;
            };
        }
    }
}