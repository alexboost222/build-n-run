using System;

namespace FSM
{
    public class InvalidState : Exception
    {
        
    }

    public class RouteAlreadyLeadsToAnotherState : Exception
    {
    }
}