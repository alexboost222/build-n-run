using System;
using System.Collections.Generic;
using FSM;
using UniRx;

public class LocalFSM<TState, TCommand> 
    where TState : Enum
    where TCommand : Enum
{
    public IReadOnlyReactiveProperty<TState> CurrentState => _currentState;
    
    private ReactiveProperty<TState> _currentState;
        
    private readonly Dictionary<(TState, TCommand), TState> _transitionRoutes = new();
        
    private readonly CreateListDictionary<TState, Action> _onEnter = new();
    private readonly CreateListDictionary<(TState, TState), Action> _transitionsTable = new();
    private readonly CreateListDictionary<TState, Action> _onExit = new();
        
    private bool _throwOnInvalidStateForCommand = true;

    public static LocalFSM<TState, TCommand> CreateInstance(TState state, bool throwOnInvalidStateForCommand = true)
    {
        return new LocalFSM<TState, TCommand>
        {
            _currentState = new ReactiveProperty<TState>(state),
            _throwOnInvalidStateForCommand = throwOnInvalidStateForCommand,
        };
    }

    public void PushCommand(TCommand cmd)
    {
        if (_transitionRoutes.TryGetValue((_currentState.Value, cmd), out var nextState))
        {
            _onExit.GetValue(_currentState.Value).ForEach(a => a.Invoke());
            _transitionsTable.GetValue((_currentState.Value, nextState)).ForEach(a => a.Invoke());
            _onEnter.GetValue(nextState).ForEach(a => a.Invoke());

            _currentState.Value = nextState;
        }
        else if (_throwOnInvalidStateForCommand)
        {
            throw new InvalidState();
        }
    }


    public LocalFSM<TState, TCommand> RegisterTransition(TState from, TState to, TCommand command, params Action[] callbacks)
    {
        if (_transitionRoutes.TryGetValue((from, command), out var nextState) && !nextState.Equals(to))
            throw new RouteAlreadyLeadsToAnotherState();
            
        _transitionRoutes[(from, command)] = to;
        _transitionsTable.Add((from, to), callbacks);
        return this;
    }

    public LocalFSM<TState, TCommand> RegisterOnEnter(TState state, params Action[] callbacks)
    {
        _onEnter.Add(state, callbacks);
        return this;
    }

    public LocalFSM<TState, TCommand> RegisterOnExit(TState state, params Action[] callbacks)
    {
        _onExit.Add(state, callbacks);
        return this;
    }
}