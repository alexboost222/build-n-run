using System;
using System.Collections.Generic;
using NaughtyAttributes;
using UniRx;
using Unity.Netcode;
using UnityEngine;

namespace FSM
{
    public class NetworkingFSM<TState, TCommand> : NetworkBehaviour 
    where TState : Enum
    where TCommand : Enum 
{
    
    // DO NOT TOUCH THIS ONLY FOR DEBUG
    public NetworkVariable<TState> CurrentState = new();
        
    private readonly Dictionary<(TState, TCommand), TState> _transitionRoutes = new();
        
    private readonly CreateListDictionary<TState, Action> _onEnter = new();
    private readonly CreateListDictionary<(TState, TState), Action> _transitionsTable = new();
    private readonly CreateListDictionary<TState, Action> _onExit = new();
    
    public void PushCommand(TCommand cmd)
    {
        if (!_transitionRoutes.TryGetValue((CurrentState.Value, cmd), out var nextState)) return;
        
        _onExit.GetValue(CurrentState.Value).ForEach(a => a.Invoke());
        _transitionsTable.GetValue((CurrentState.Value, nextState)).ForEach(a => a.Invoke());
        _onEnter.GetValue(nextState).ForEach(a => a.Invoke());

        CurrentState.Value = nextState;
    }


    public NetworkingFSM<TState, TCommand> RegisterTransition(TState from, TState to, TCommand command, params Action[] callbacks)
    {
        if (_transitionRoutes.TryGetValue((from, command), out var nextState) && !nextState.Equals(to))
            throw new RouteAlreadyLeadsToAnotherState();
            
        _transitionRoutes[(from, command)] = to;
        _transitionsTable.Add((from, to), callbacks);
        return this;
    }

    public NetworkingFSM<TState, TCommand> RegisterOnEnter(TState state, params Action[] callbacks)
    {
        _onEnter.Add(state, callbacks);
        return this;
    }

    public NetworkingFSM<TState, TCommand> RegisterOnExit(TState state, params Action[] callbacks)
    {
        _onExit.Add(state, callbacks);
        return this;
    }
}
}