using System;
using System.Linq;
using System.Reflection;
using UniRx;
using UnityEngine;

namespace FSM
{
    public class ReflectionFSM<TState, TCommand> 
        where TState : Enum
        where TCommand : Enum
    {
        public IReadOnlyReactiveProperty<TState> CurrentState => LocalFSM.CurrentState;

        protected LocalFSM<TState, TCommand> LocalFSM;
        public static T CreateInstance<T>(TState state) where T : ReflectionFSM<TState, TCommand>
        {
            var localFSM = LocalFSM<TState, TCommand>.CreateInstance(state, false);
            var instance = Activator.CreateInstance<T>();
            
            
            var methods = typeof(T)
                .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            
            foreach (var method in methods)
            {
                var transitionAttributes = method.GetCustomAttributes(typeof(TransitionAttribute), false);
                var onEnterAttributes = method.GetCustomAttributes(typeof(OnEnterAttribute), false);
                var onExitAttributes = method.GetCustomAttributes(typeof(OnExitAttribute), false);

                foreach (TransitionAttribute transition in transitionAttributes)
                {
                    if (transition.From is not TState from ||
                        transition.To is not TState to ||
                        transition.Command is not TCommand command)
                        throw new InvalidCastException();
                    
                    var action = (Action)Delegate.CreateDelegate(typeof(Action), instance, method.Name);
                    localFSM.RegisterTransition(from, to, command, action);
                }

                foreach (OnEnterAttribute onEnter in onEnterAttributes)
                {
                    if (onEnter.State is not TState enter)
                        throw new InvalidCastException();
                    
                    var action = (Action)Delegate.CreateDelegate(typeof(Action), instance, method.Name);
                    localFSM.RegisterOnEnter(enter, action);
                }

                foreach (OnExitAttribute onExit in onExitAttributes)
                {
                    
                    if (onExit.State is not TState exit)
                        throw new InvalidCastException();
                    
                    var action = (Action)Delegate.CreateDelegate(typeof(Action), instance, method.Name);
                    localFSM.RegisterOnEnter(exit, action);
                }
            }

            instance.LocalFSM = localFSM;
            return instance;
        }
        
        
        
        public void PushCommand(TCommand cmd)
        {
            LocalFSM.PushCommand(cmd);
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class TransitionAttribute : Attribute
    {
        public object From { get; }
        public object To { get; }
        public object Command { get; }

        public TransitionAttribute(object from, object to, object command)
        {
            From = from;
            To = to;
            Command = command;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class OnEnterAttribute : Attribute
    {
        public object State { get; }

        public OnEnterAttribute(object state)
        {
            State = state;
        }
    }
    
    [AttributeUsage(AttributeTargets.Method)]
    public class OnExitAttribute : Attribute
    {
        public object State { get; }

        public OnExitAttribute(object state)
        {
            State = state;
        }
    }
}