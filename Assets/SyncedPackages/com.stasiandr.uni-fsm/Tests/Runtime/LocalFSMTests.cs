using FSM;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Runtime
{
    public class LocalFSMTests
    {
        [Test]
        public void SimplePasses()
        {
            var fsm = LocalFSM<Scene, Command>.CreateInstance(Scene.EntryPoint)
                .RegisterTransition(Scene.EntryPoint, Scene.Loading, Command.StartLoading, () => Debug.Log("Loading"))
                .RegisterTransition(Scene.Loading, Scene.MainScene, Command.LoadToMainScene, () => Debug.Log("Transiting To Main scene"))
                .RegisterOnEnter(Scene.Loading, () => Debug.Log("Entering Loading"))
                .RegisterOnExit(Scene.Loading, () => Debug.Log("Exiting Loading"));

            fsm.PushCommand(Command.StartLoading);
            fsm.PushCommand(Command.LoadToMainScene);
        }
    }
}
