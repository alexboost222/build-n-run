using System;
using FSM;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Runtime
{
    public class ReflectionFSMTests
    {
        public class SceneFSM : ReflectionFSM<Scene, Command>
        {
            [Transition(Scene.EntryPoint, Scene.Loading, Command.StartLoading)]
            public void LoadingFromEntryPointTransition()
            {
                Debug.Log("Loading");
            }
            
            [Transition(Scene.Loading, Scene.MainScene, Command.LoadToMainScene)]
            public void LoadingToMainSceneTransition()
            {
                Debug.Log("Transiting To Main scene");
            }
            
            [OnEnter(Scene.Loading)]
            public void OnEnterLoading()
            {
                Debug.Log("Entering Loading");
            }

            [OnExit(Scene.Loading)]
            public void OnExitLoading()
            {
                Debug.Log("Exiting Loading");
            }
        }
        
        [Test]
        public void SimplePasses()
        {
            var fsm = SceneFSM.CreateInstance<SceneFSM>(Scene.EntryPoint);

            fsm.PushCommand(Command.StartLoading);
            fsm.PushCommand(Command.LoadToMainScene);
        }
    }

    
}