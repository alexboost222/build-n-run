namespace Tests.Runtime
{
    public enum Scene : int
    {
        EntryPoint,
        Loading,
        MainScene,
    }

    public enum Command
    {
        StartLoading,
        LoadToMainScene,
    }
}